export const DEF_COLOR = "#f0f0f04d" as const;
export const FIRST_COLOR = "#d87676cc" as const;
export const SECOND_COLOR = "#3c7db3cc" as const;

export type Color = typeof FIRST_COLOR | typeof SECOND_COLOR | typeof DEF_COLOR;
