import React from "react";

import { styled } from "@mui/system";
import { Color, DEF_COLOR } from "../../utils/colors";

interface Props {
  children: React.ReactChild | React.ReactChild[];
  backgroundColor?: Color;
}

const StyledBox = styled("div")({
  width: 100,
  height: 100,
  borderRadius: 8,
  borderColor: "color",
  borderStyle: "solid",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  textAlign: "center",
  padding: 12,
});

export const Child: React.FC<Props> = ({
  children,
  backgroundColor = DEF_COLOR,
}: Props) => {
  return <StyledBox style={{ backgroundColor }}>{children}</StyledBox>;
};
