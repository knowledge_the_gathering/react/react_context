import { styled } from "@mui/system";
import React from "react";

interface Props {
  children: React.ReactChild | React.ReactChild[];
}

const StyledContainer = styled("div")({
  width: "100%",
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-around",
});

export const Row: React.FC<Props> = ({ children }: Props) => {
  return <StyledContainer>{children}</StyledContainer>;
};
