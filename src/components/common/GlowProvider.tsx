import { styled } from "@mui/system";
import React, { useCallback, useContext, useState } from "react";

type Glow = () => void;

interface GlowWrapperProps {
  children: React.ReactChild | React.ReactChild[];
}

export const GlowContext = React.createContext<Glow>(() => {});

const StyledBox = styled("div")({
  borderRadius: 8,
  margin: 24,

  "@keyframes pulse": {
    "0%": {
      " -moz-box-shadow": "0 0 0 0 rgba(0,0,0, 0.8)",
      "box-shadow": "0 0 0 0 rgba(0,0,0, 0.8)",
    },
    "70%": {
      "-moz-box-shadow": "0 0 0 8px rgba(0,0,0, 0.2)",
      "box-shadow": "0 0 0 8px rgba(0,0,0, 0.2)",
    },
    "100%": {
      "-moz-box-shadow": "0 0 0 0 rgba(0,0,0, 0)",
      "box-shadow": "0 0 0 0 rgba(0,0,0, 0)",
    },
  },
});

const ANIMATION_TIME: number = 0.6;

export const GlowProvider: React.FC<GlowWrapperProps> = ({
  children,
}: GlowWrapperProps) => {
  const [count, setCount] = useState(0);

  const glow = useCallback(() => {
    setCount((prev) => {
      const next = prev + 1;
      return next;
    });
    setTimeout(() => {
      setCount((prev) => {
        const next = prev > 0 ? prev - 1 : prev;
        return next;
      });
    }, ANIMATION_TIME * 1000);
  }, [setCount]);

  return (
    <GlowContext.Provider value={glow}>
      <StyledBox
        style={{
          ...(count > 0
            ? { animation: `pulse ${ANIMATION_TIME}s infinite ease` }
            : {}),
        }}
      >
        {children}
      </StyledBox>
    </GlowContext.Provider>
  );
};

export const useGlow = () => {
  const glow = useContext(GlowContext);
  return { glow };
};
