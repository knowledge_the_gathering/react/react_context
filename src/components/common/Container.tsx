import { styled } from "@mui/system";
import React from "react";

interface Props {
  children: React.ReactChild | React.ReactChild[];
}

const StyledContainer = styled("div")({
  minWidth: 800,
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  margin: 12,
  borderRadius: 8,
  padding: 24,
  backgroundColor:'white'

});

export const Container: React.FC<Props> = ({ children }: Props) => {
  return <StyledContainer>{children}</StyledContainer>;
};
