import { styled } from "@mui/system";
import React from "react";

interface Props {
  children: React.ReactChild | React.ReactChild[];
  flex?: number;
  alignItems?: string;
}

const StyledContainer = styled("div")({
  width: "100%",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "flex-start",
});

export const Column: React.FC<Props> = ({
  children,
  flex = 1,
  alignItems = "center",
}: Props) => {
  return (
    <StyledContainer style={{ flex, alignItems }}>{children}</StyledContainer>
  );
};
