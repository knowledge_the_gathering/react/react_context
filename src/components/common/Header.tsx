import { styled } from "@mui/system";
import React from "react";

interface Props {
  children: React.ReactChild | React.ReactChild[];
}

const StyledText = styled("div")({
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-around",
  fontWeight: "800",
  fontSize: 24,
  marginBottom: 24,
});

export const Header: React.FC<Props> = ({ children }: Props) => {
  return <StyledText>{children}</StyledText>;
};
