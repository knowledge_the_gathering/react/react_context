import React from "react";

import { Button } from "@mui/material";

interface Props {
  onPress: () => void;
}

export const TriggerButton: React.FC<Props> = ({ onPress }: Props) => {
  return (
    <Button
      style={{
        color: "#2552552550d",
        backgroundColor: "#000000",
        width: 100,
        minWidth: 100,
        maxWidth: 100,
        boxSizing: "content-box",
      }}
      onClick={onPress}
    >{`Trigger`}</Button>
  );
};
