import { lineHeight, styled } from "@mui/system";
import React from "react";

interface Props {
  children: React.ReactChild | React.ReactChild[];
}

const StyledText = styled("div")({
  display: "flex",
  "white-space": "pre-wrap",
  fontFamily: "arial",
  borderRadius: 8,
  padding: 42,
  fontSize: 21,
  lineHeight: '42px'
});

export const Paragraph: React.FC<Props> = ({ children }: Props) => {
  return <StyledText>{children}</StyledText>;
};