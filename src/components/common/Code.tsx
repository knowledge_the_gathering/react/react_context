import { styled } from "@mui/system";
import React from "react";

interface Props {
  children: React.ReactChild | React.ReactChild[];
}

const StyledText = styled("div")({
  backgroundColor: "#eee",
  display: "flex",
  "white-space": "pre-wrap",
  fontFamily: "arial",
  borderRadius: 8,
  padding: 42,
});

export const Code: React.FC<Props> = ({ children }: Props) => {
  return <StyledText>{children}</StyledText>;
};