import React, { useContext, useEffect, useState } from "react";

import { Container } from "../common/Container";
import { Column } from "../common/Column";
import { Header } from "../common/Header";
import { Code } from "../common/Code";

import { TriggerButton } from "../common/TriggerButton";
import { Child } from "../common/Child";
import { GlowProvider, useGlow } from "../common/GlowProvider";

import {
  FIRST_COLOR,
  SECOND_COLOR,
  DEF_COLOR,
  Color,
} from "../../utils/colors";

const DirectContext = React.createContext<Color>(DEF_COLOR);

const ChildConsumer = () => {
  const color = useContext(DirectContext);
  const { glow } = useGlow();
  useEffect(() => {
    glow();
  });
  return <Child backgroundColor={color}>Context Consumer</Child>;
};

const ChildNonConsumer = () => {
  const { glow } = useGlow();

  useEffect(() => {
    glow();
  });

  return <Child>Non Consumer</Child>;
};

const Chidlren: React.FC = () => {
  return (
    <>
      <GlowProvider>
        <ChildConsumer />
      </GlowProvider>
      <GlowProvider>
        <ChildNonConsumer />
      </GlowProvider>
    </>
  );
};

const MemoizedChildren = React.memo(Chidlren);

export const Memoized = () => {
  const [color, setColor] = useState<Color>(FIRST_COLOR);

  const toggle = () => {
    setColor(
      (prev: Color): Color =>
        prev === FIRST_COLOR ? SECOND_COLOR : FIRST_COLOR
    );
  };

  return (
    <DirectContext.Provider value={color}>
      <Container>
        <Column flex={3} alignItems={"flex-start"}>
          <Header>{"React.memo"}</Header>
          <Code>
            {`const Context = React.createContext('some_value');\n
const Consumer = () => {
    const value = useContext(Context);
    return (<View />);
}\n
const NonConsumer = () => {
    return (<View />);
}\n
const MemoizedConsumer = () => {
    return ( 
        <React.Fragment>      
            <Consumer />
            <NonConsumer />
        </React.Fragment>
    );
}\n
const Memoized = React.memo(MemoizedConsumer)\n
const Parent = () => {
    const [value, setValue] = useState('value');
    const doStuff = () => setValue("new value") \n
    return (
        <Context.Provider value={value}>
            <Button onPress={doStuff}>
            <MemoizedConsumer />
        </Context>
    );
}`}
          </Code>
        </Column>

        <Column>
          <TriggerButton onPress={toggle} />
          <MemoizedChildren />
        </Column>
      </Container>
    </DirectContext.Provider>
  );
};
