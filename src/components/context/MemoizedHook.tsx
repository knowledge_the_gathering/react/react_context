import React, { useContext, useEffect, useMemo, useState } from "react";

import { Container } from "../common/Container";
import { Column } from "../common/Column";
import { Header } from "../common/Header";
import { Code } from "../common/Code";

import { TriggerButton } from "../common/TriggerButton";
import { Child } from "../common/Child";
import { GlowProvider, useGlow } from "../common/GlowProvider";

import {
  FIRST_COLOR,
  SECOND_COLOR,
  DEF_COLOR,
  Color,
} from "../../utils/colors";

const DirectContext = React.createContext<Color>(DEF_COLOR);

const ChildConsumer = () => {
  const color = useContext(DirectContext);
  const { glow } = useGlow();
  useEffect(() => {
    glow();
  });
  return <Child backgroundColor={color}>Context Consumer</Child>;
};

const ChildNonConsumer = () => {
  const { glow } = useGlow();

  useEffect(() => {
    glow();
  });

  return <Child>Non Consumer</Child>;
};

export const MemoizedHook = () => {
  const [color, setColor] = useState<Color>(FIRST_COLOR);

  const toggle = () => {
    setColor(
      (prev: Color): Color =>
        prev === FIRST_COLOR ? SECOND_COLOR : FIRST_COLOR
    );
  };

  const memoized = useMemo(() => {
    return (
      <>
        <GlowProvider>
          <ChildConsumer />
        </GlowProvider>
        <GlowProvider>
          <ChildNonConsumer />
        </GlowProvider>
      </>
    );
  }, []);

  return (
    <DirectContext.Provider value={color}>
      <Container>
      <Column flex={3} alignItems={"flex-start"}>
          <Header>{"useMemo"}</Header>
          <Code>
            {`const Context = React.createContext('some_value');\n
const Consumer = () => {
    const value = useContext(Context);
    return (<View />);
}\n
const NonConsumer = () => {
    return (<View />);
}\n
const Parent = () => {
    const [value, setValue] = useState('value');
    const doStuff = () => setValue("new value");
    const memoized = useMemo(() => {
        return ( 
            <React.Fragment>      
                <Consumer />
                <NonConsumer />
            </React.Fragment>
        );
    }, []);\n
    return (
        <Context.Provider value={value}>
            <Button onPress={doStuff}>
            {memoized}
        </Context>
    );
}`}
          </Code>
        </Column>

        <Column>
          <TriggerButton onPress={toggle} />
          {memoized}
        </Column>
      </Container>
    </DirectContext.Provider>
  );
};
