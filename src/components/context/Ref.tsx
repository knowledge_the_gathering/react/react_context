import React, { useContext, useEffect, useRef } from "react";

import { Container } from "../common/Container";
import { Column } from "../common/Column";
import { Header } from "../common/Header";
import { Code } from "../common/Code";

import { TriggerButton } from "../common/TriggerButton";
import { Child } from "../common/Child";
import { GlowProvider, useGlow } from "../common/GlowProvider";

import {
  FIRST_COLOR,
  SECOND_COLOR,
  DEF_COLOR,
  Color,
} from "../../utils/colors";

const DirectRefContext = React.createContext<Color>(DEF_COLOR);

const ChildConsumer = () => {
  const color = useContext(DirectRefContext);

  const { glow } = useGlow();
  useEffect(() => {
    glow();
  });
  return <Child backgroundColor={color}>Context Consumer</Child>;
};

const ChildNonConsumer = () => {
  const { glow } = useGlow();

  useEffect(() => {
    glow();
  });

  return <Child>Non Consumer</Child>;
};

export const Ref = () => {
  const colorRef = useRef<Color>(FIRST_COLOR);

  const toggle = () => {
    colorRef.current =
      colorRef.current === FIRST_COLOR ? SECOND_COLOR : FIRST_COLOR;
  };

  return (
    <DirectRefContext.Provider value={colorRef.current}>
      <Container>
        <Column flex={3} alignItems={"flex-start"}>
          <Header>{"useRef"}</Header>
          <Code>
            {`const Context = React.createContext('some_value');\n
const Consumer = () => {
    const value = useContext(Context);
    return (<View />);
}\n
const NonConsumer = () => {
    return (<View />);
}\n
const Parent = () => {
    const ref = useRef('value');
    const doStuff = () => ref.current = "new value"; \n
    return (
        <Context.Provider value={ref.current}>
            <Button onPress={doStuff}>
            <Consumer />
            <NonConsumer />
        </Context>
    );
}`}
          </Code>
        </Column>
        <Column>
          <TriggerButton onPress={toggle} />

          <GlowProvider>
            <ChildConsumer />
          </GlowProvider>
          <GlowProvider>
            <ChildNonConsumer />
          </GlowProvider>
        </Column>
      </Container>
    </DirectRefContext.Provider>
  );
};
