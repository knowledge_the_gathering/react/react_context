import React, { useContext, useEffect, useReducer, useState } from "react";

import { Container } from "../common/Container";
import { Column } from "../common/Column";
import { Header } from "../common/Header";
import { Code } from "../common/Code";

import { TriggerButton } from "../common/TriggerButton";
import { Child } from "../common/Child";
import { GlowProvider, useGlow } from "../common/GlowProvider";

import { FIRST_COLOR, SECOND_COLOR, Color } from "../../utils/colors";

const HR_TOGGLE = "hr/TOGGLE" as const;

interface ContextProviderProps {
  children: React.ReactChild | React.ReactChild[];
}

interface ContextProps {
  color: Color;
  toggle: () => void;
}

function colorReducer(state: Color, { type }: { type: string }) {
  switch (type) {
    case HR_TOGGLE: {
      return state === FIRST_COLOR ? SECOND_COLOR : FIRST_COLOR;
    }
    default:
      return state;
  }
}

const ProviderContext = React.createContext<ContextProps>({
  color: FIRST_COLOR,
  toggle: () => {},
});

const ContextProvider = ({ children }: ContextProviderProps) => {
  const [color, dispatch] = useReducer(colorReducer, FIRST_COLOR);
  const toggle = () => {
    dispatch({ type: HR_TOGGLE });
  };

  return (
    <ProviderContext.Provider value={{ color, toggle }}>
      {children}
    </ProviderContext.Provider>
  );
};

const useColor = () => {
  const { color, toggle } = useContext(ProviderContext);
  return {
    color,
    toggle,
  };
};

const ContextToggleButton: React.FC = () => {
  const { toggle } = useColor();
  return <TriggerButton onPress={toggle} />;
};

const ChildConsumer = () => {
  const { color } = useColor();
  const { glow } = useGlow();
  useEffect(() => {
    glow();
  });
  return <Child backgroundColor={color}>Context Consumer</Child>;
};

const ChildNonConsumer = () => {
  const { glow } = useGlow();

  useEffect(() => {
    glow();
  });

  return <Child>Non Consumer</Child>;
};

export const Provider = () => {
  return (
    <ContextProvider>
      <Container>
        <Column flex={3} alignItems={"flex-start"}>
          <Header>{"Children as props"}</Header>
          <Code>
            {`const Context = React.createContext('some_value');\n
const Consumer = () => {
    const value = useContext(Context);
    return (<View />);
}\n
const NonConsumer = () => {
    return (<View />);
}\n
const Button = () => {
    const { value, toggle } = useContext(Context);
    return <Button onPress={doStuff}>
}\n
const Provider = ({children}) => {
    const [value, setValue] = useState('value');
    const doStuff = () => setValue("new value");\n
    return (
        <Context.Provider value={{value, toggle}}>
          {children}
        </Context>
  )
}\n
const Parent = () => {
    return (
        <Provider>
            <Button />
            <Consumer />
            <NonConsumer />
        </Provider>
    );
}`}
          </Code>
        </Column>

        <Column>
          <ContextToggleButton />

          <GlowProvider>
            <ChildConsumer />
          </GlowProvider>
          <GlowProvider>
            <ChildNonConsumer />
          </GlowProvider>
        </Column>
      </Container>
    </ContextProvider>
  );
};
