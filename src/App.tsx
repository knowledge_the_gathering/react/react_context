import React from "react";

import { Container } from "./components/common/Container";
import { Paragraph } from "./components/common/Paragraph";

import { Direct } from "./components/context/Direct";
import { Ref } from "./components/context/Ref";
import { Provider } from "./components/context/Provider";
import { Memoized } from "./components/context/Memoized";
import { MemoizedHook } from "./components/context/MemoizedHook";

import { styled } from "@mui/system";

const AppContainer = styled("div")({
  flex: 1,
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
  backgroundColor: "#0000000d",
  padding: 42,
});

export const App = () => {
  return (
    <AppContainer>
      <Container>
        <Paragraph>
          {`React Context is a method to pass props from parent to child component(s), by storing the props in a store(similar in Redux) and using these props from the store by child component(s) without actually passing them manually at each level of the component tree.`}
        </Paragraph>
      </Container>
      <Direct />
      <Container>
        <Paragraph>
          {`React's default behavior is that when a parent component renders, React will recursively render all child components inside of it!`}
        </Paragraph>
      </Container>
      <Ref />
      <Container>
        <Paragraph>
          {`State updates in React should always be done immutably.
If you mutate, then someValue is the same reference, and those components will assume nothing has changed.`}
        </Paragraph>
      </Container>
      <Provider />
      <Container>
        <Paragraph>
          {`Assumption: for render() child component is defined not where component is positioned in the try, but whom it was rendered`}
        </Paragraph>
      </Container>
      <Memoized />
      <MemoizedHook />
    </AppContainer>
  );
};
