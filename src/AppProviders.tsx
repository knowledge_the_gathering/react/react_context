import React from "react";
import { render } from "react-dom";
import { ThemeProvider } from "@mui/material";

import { App } from "./App";

import { theme } from "./utils/theme";

interface Props {
  children?: React.ReactChild | React.ReactChild[];
}

const AppProviders: React.FC<Props> = ({ children }: Props) => {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export const init = () => {
  render(
    <AppProviders>
      <App />
    </AppProviders>,
    document.getElementById("root")
  );
};
